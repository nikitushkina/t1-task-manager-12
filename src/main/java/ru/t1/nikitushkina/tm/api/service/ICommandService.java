package ru.t1.nikitushkina.tm.api.service;

import ru.t1.nikitushkina.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
